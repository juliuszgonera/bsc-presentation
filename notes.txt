1. Wprowadzenie do tematyki pracy (5-10 minut)

- inne protokoły
  + corba
    - tcp/ip
    - trudne
  + soap
2. Cel pracy (5-10 minut)
3. Zamierzenia dot pracy
  - znane rozwiązania
  - element nowości
  - wymagania minimalne
    + jako user mogę wyświetlić listę zadań do wykonania dziś/w tym
      tygodniu/zbiorczą
    + jako user mogę utworzyć nowe zadanie, podając jego treść, datę
      wykonania i priorytet
    + lista zadań jest posortowana wg priorytetu zadania
    + system ma umożliwiać pracę offline
    + system ma automatycznie synchronizować listę zadań z serwerem

  - wymagania dopełniające
4. Dotychczasowe wyniki
  - co jest zrobione
  - z czym się zapoznałem
